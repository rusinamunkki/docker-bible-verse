var url = "https://raamattu.uskonkirjat.net/servlet/biblesite.Daily";
var https = require('https');

function getDataFromURL(url) {
    var request = https.get(url, function(res) {
        res.setEncoding('latin1');
        var body = '';

        res.on('data', function(chunk) {
            body += chunk;
        });

        res.on('end', function() {
            handleResult(body);
        });
    });
}

function handleResult(data) {
    var res = data.split('>');
    var verse = res[21];
    var position = res[24];

    position = position.replace('&nbsp;', '');
    position = position.substr(0, position.length-3);
    verse = verse.substr(0, verse.length-3);

    console.log(position + " " + verse);
}

getDataFromURL(url);
