FROM alpine
RUN apk update \
    && apk upgrade \
    && apk add nodejs

WORKDIR /app
ADD . /app
ENTRYPOINT ["node", "bibleverse.js"]
