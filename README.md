# docker-bible-verse #

Docker for showing daily bible verse in finnish language.

### Installation and testing ###

* Clone source
* Build image
 docker build -t stargazers/bible-verse .
* Run in docker
 docker --rm stargazers/bible-verse
